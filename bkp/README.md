**Pgto Split Pay**

API utilizada para servir a aplicação web e mobile com o objetivo de realizar a divisão dos pagamentos realizados nas clínicas da PAX Prever.

**Stack**
* nodejs
* express
* typescript
* SQL Server
