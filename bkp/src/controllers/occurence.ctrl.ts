import Ocurrence, { IOccurrence } from '../models/occurence';
import { Request, Response } from "express";

export default class OccurenceController {

  public async get(req: Request, res: Response) {
    const id = req.params.id;
    const entity = await Ocurrence.findById(id);
    return res.json(entity);
  }

  public async getAll(req: Request, res: Response) {
    const list = await Ocurrence.find();
    return res.json(list);

  }

  public async post(req: Request, res: Response) {
    const entityBody = req.body;
    const entityBase = new Ocurrence(entityBody);
    await entityBase.save();
    return res.json(entityBase);
  }

  public async put(req: Request, res: Response) {
    return res.json();
  }

  public async del(req: Request, res: Response) {
    const id = req.params.id;
    const entity = await Ocurrence.findByIdAndDelete(id);
    return res.json(entity);
  }
}
