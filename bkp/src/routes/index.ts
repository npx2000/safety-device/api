import { Router } from "express";
import occurrence from "./occurrence.route";

const router = Router();

router.get("/", function (req, res) { res.json({ title: "API", version: "1.0.0", env: process.env.NODE_ENV }); });
router.use("/occurrence", occurrence);

export default router;
