import { Router } from 'express';
import OccurenceController from '../controllers/occurence.ctrl';

const router = Router();
const controller = new OccurenceController();

router.get('/:id', controller.get);
router.get('/', controller.getAll);
router.post('/', controller.post);
router.put('/:id', controller.put);
router.delete('/:id', controller.del);

export default router;
