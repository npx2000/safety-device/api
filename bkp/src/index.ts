import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import router from "./routes";
import config from "./config";

const app = express();

app.use(cors());
app.use(express.json());
app.use(router);

const port = process.env.PORT || 3001;

mongoose.connect(config.DABABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then(() => {
        return console.info(`Successfully connected to database.`);
    })
    .catch((error: any) => {
        console.error('Error connecting to database: ', error);
        return process.exit(1);
    });

app.listen(port, () => console.log(`The server is running at localhost: ${port}.`));
