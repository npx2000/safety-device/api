import mongoose, { Schema, Document } from 'mongoose';

export interface IOccurrence extends Document {
  deviceId: string;
  priority: string;
  location: number[];

}

const OccurrenceSchema: Schema = new Schema({
  deviceId: { type: String, required: true },
  priority: { type: String, required: true },
  location: [{ type: Number, index: '2dsphere', required: true }],
});

export default mongoose.model<IOccurrence>('Ocurrence', OccurrenceSchema);