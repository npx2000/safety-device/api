## Instalação - Ambiente de desenvolvimento local

- Estamos usando o `dotenv-safe`, ou seja, abra o arquivo `.env.example`, veja todas as variáveis de ambiente **sigilosas** 
que precisam ser criadas para este projeto e então, crie um arquivo `.env` na raíz do mesmo com as chaves e os valores desejados. 
> O arquivo `.env` não deve ser comitado juntamente com o projeto (ele está adicionado no .gitignore).

- Outras configuarações **não sigilosas** podem ser armazenados nos arquivos como, por exemplo, 
`development.json` e `production.json` no diretório `config`.

- Instale o pacote `pm2` globalmente com `npm i -g pm2` e, em seguida crie um link simbólico _(symlink)_ dele para esse 
projeto com `npm link pm2`.

- Instale os demais pacotes com `npm i` na raíz do projeto.

- *[Apenas para desenvolvimento]* Execute `docker-compose up -d` no nível raiz deste projeto para executar os serviços 
`mongo` e `mongo-express` via docker.

- Em seguida, você pode executar todas as tarefas necessárias de gerenciamento **local** do mongodb para este projeto através do link `http://localhost:8081`.

- Execute a aplicação com `npm run dev`.

Você pode testar a api através do link `http://localhost:5000/v1/api-docs/` ou se preferir, 
importar a `collection` do `POSTMAN`, `SafetyDeviceAPI.postman_collection`.

> Também haverá uma coleção do `Postman` na raíz desse projeto que poderá ser importado. 

> Note: Para o ambiente de desenvolvimento estamos utilizando o `nodemon` (e não o `pm2`, mas é possível testá-lo 
>localmente apontando para o ambiente de *Produção* ou outro, como *Homologação* caso necessário).

> Se preferir não trabalhar com `docker`, crie todos os recursos necessários para o desenvolvimento e testes locais
  (na máquina do desenvolvedor) e adeque as configurações conforme necessário.


## Instalação - Ambiente de Produção

- Estamos usando o `dotenv-safe`, ou seja, abra o arquivo `.env.example`, veja todas as variáveis de ambiente **sigilosas** 
que precisam ser criadas para este projeto e então, crie um arquivo `.env` na raíz do mesmo com as chaves e os valores desejados. 
> O arquivo `.env` não deve ser comitado juntamente com o projeto (ele está adicionado no .gitignore).

- Outras configuarações **não sigilosas** podem ser armazenados nos arquivos como, por exemplo, 
`development.json` e `production.json` no diretório `config`.

- Instale o pacote `pm2` globalmente com `npm i -g pm2` e, em seguida crie um link simbólico _(symlink)_ dele para esse 
projeto com `npm link pm2`.

- O script `npm start` agora executará `pm2 start ./pm2.config.js --env=production`, ou seja, foi reservado para iniciar 
a aplicação em **Produção**.

> Todas as configurações do `PM 2` estão no arquivo `pm2.config.js`.

> Todas os scripts necessários para as tarefas do dia-a-dia estão incluídos na seção `scripts` do `package.json`.

> No ambiente de **Produção** é preciso jogar manualmente no servidor de produção, na raíz do projeto, um arquivo `.env`, assim como feito no 
  ambiente de desenvolvimento.

## Health Check and Graceful Shutdown

- Health Checks - Para sinalizar aos seus load balancers que não há necessidade de reiniciar o servidor,  
verificando o que você julgar necessário para considerar que sua aplicação está funcionando plenamente, 
como por exemplo, encerrar conexões com o banco de dados.

- Graceful Shutdown - Para ter a chance de descartar todos os seus recursos não gerenciados pelo Node.js 
antes que seu aplicativo seja morto, como por exemplo, encerrar conexões com o banco de dados.


## Configurações do Projeto

Para as configurações, este projeto trabalha em que uma combinação de `dotenv` (usando` dotenv-safe`) e `convict`, 
portanto, todos os valores sigilosos que devem ser armazenados como variáveis de ambiente estão no arquivo `.env.example` 
e os outros valores que não há problema em revelá-los com outros membros da equipe, podem ser armazenados em arquivos
 como `development.json` e `production.json` no diretório `config`.
 
 No ambiente de **Produção** é preciso jogar manualmente, na raíz do projeto um arquivo `.env`, assim como feito no 
 ambiente de desenvolvimento.
  
 ## Notificações Push com o Firebase
 
- Criar o projeto e seguir as devidas documentações do próprio Firebase sobre o *FCM - Firebase Cloud Messaging* e 
como implantar o *Firebase Admin SDK* no servidor.

- Fazer download do arquivo de configuração do projeto que será utilizado no servidor pelo *Firebase Admin SDK* com o nome `serviceAccountKey` e
deixá-lo na raíz do projeto.
> Esse arquivo está incluído no `.gitignore` por razões de segurança

- Habilitar a API *Firebase Cloud Messaging API* no dashboard do Google APIs
> Ao tentar enviar a notificação pela primeira vez, uma exceção será disparada relatando esse problema e
> com o link direto para essa ação.
>

Para esta solução, ao registrar uma ocorrência estamos enviando uma notificação push (FCM) 
para um **tópico** chamado `occurrences`, portanto, para os aplicativos clientes que desejarem receber tais notificações, 
precisarão se "subscrever" neste tópico.
 
Segue documentação: [Subscribe and unsubscribe using the Firebase Admin SDK](https://firebase.google.com/docs/cloud-messaging/manage-topics?authuser=0#subscribe-and-unsubscribe-using-the-adminSDK)
 

