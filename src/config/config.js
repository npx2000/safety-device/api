const convict = require('convict');

const config = convict({
  stage: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  mongodbConfig: {
    connectionString: {
      doc: 'Main Database Connection String',
      format: String,
      default: '',
      env: 'MONGODB_CONNSTR',
    },
  },
  jwtConfig: {
    secret: {
      doc: 'Secret used to encrypt/decrypt JWT tokens (should not be shared)',
      format: '*',
      default: null,
      env: 'AUTH_SECRET',
    },
    exp: {
      doc: 'Expiration token time (time in epoch)',
      format: 'int',
      default: null,
    },
  },
  safetyDeviceApiConfig: {
    port: {
      doc: 'API Port',
      format: 'port',
      default: 3001,
      env: 'PORT',
    },
    baseUri: {
      doc: 'Base Url as follows: https://localhost:4000/v1',
      format: String,
      default: null,
    },
  },
  firebaseConfig: {
    fmcUrl: {
      doc: '',
      format: String,
      default: 'https://fcm.googleapis.com/fcm/send',
      env: 'FIREBASE_FMC_URL',
    },
    fmcHost: {
      doc: '',
      format: String,
      default: "fcm.googleapis.com",
      env: 'FIREBASE_FMC_HOST',
    },
    fmcKey: {
      doc: '',
      format: String,
      default: "AAAAFNdZ3xA:APA91bEQOOrePOqHL4afHYwHNohx6PoOXzpTFiLisetx3TufVm5k8VHXGplyA4du4A2lMRSiYAJ_c6-hWH6QVWGuYHDQnWcG49g3UyDkia5KhDB_hfHRTlLhSunZC0QP0_ckuH5mpFUf",
      env: 'FIREBASE_FMC_KEY',
    },

  },
  appUrl: {
    doc: '',
    format: String,
    default: 'https://dss-button.netlify.app/',
    env: 'APP_URL',
  },
});

const stage = config.get('stage');
config.loadFile(`./src/config/${stage}.json`);

module.exports = config.getProperties();
