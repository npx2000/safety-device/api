const { getUsers, getUserByID, addUser, updateUser, deleteUser, getDashboard, updateUserFCMToken } = require('../controllers/userController')
const { getOccurrences, getOccurrenceByID, addOccurrence, deleteOccurrenceByClient, } = require('../controllers/occurrenceController')
const { login, loginRequired, register } = require('../controllers/authController')
const { getProfile, updateProfile } = require('../controllers/profileController')

const handle = (promiseFn) => (req, res, next) => promiseFn(req, res, next).catch((err) => next(err))

const routes = (app) => {
  // Users
  app.route('/users')
    .get(handle(async (req, res, next) => getUsers(req, res, next)))
    .post(handle(async (req, res, next) => addUser(req, res, next)))
    .post(loginRequired, handle(async (req, res, next) => addUser(req, res, next)))

  app.route('/users/dashboard')
    .get(loginRequired, handle(async (req, res, next) => getDashboard(req, res, next)))

  app.route('/users/:userID')
    .get(loginRequired, handle(async (req, res, next) => getUserByID(req, res, next)))
    .put(loginRequired, handle(async (req, res, next) => updateUser(req, res, next)))
    .delete(loginRequired, handle(async (req, res, next) => deleteUser(req, res, next)))

  app.route('/users/fmcToken/:userID')
    .put(loginRequired, handle(async (req, res, next) => updateUserFCMToken(req, res, next)))

  // Occurrences
  app.route('/occurrences')
    .get(handle(async (req, res, next) => getOccurrences(req, res, next)))
    .post(handle(async (req, res, next) => addOccurrence(req, res, next)))

  app.route('/occurrences/client/:clientId')
    .delete(handle(async (req, res, next) => deleteOccurrenceByClient(req, res, next)))

  app.route('/occurrences/:occurrenceID')
    .get(handle(async (req, res, next) => getOccurrenceByID(req, res, next)))

  // Auth routes
  app.route('/auth/login')
    .post(handle(async (req, res, next) => login(req, res, next)))

  app.route('/auth/register')
    .post(loginRequired, handle(async (req, res, next) => register(req, res, next)))

  app.route('/profile')
    .get(loginRequired, handle(async (req, res, next) => getProfile(req, res, next)))
    .put(loginRequired, handle(async (req, res, next) => updateProfile(req, res, next)))

}

module.exports = routes
