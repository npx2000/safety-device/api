const mongoose = require('mongoose')
const { NotFoundError } = require('../lib/customerrors/applicationErrors')
const { OccurrenceSchema } = require('../models/occurrenceModel')
const { UserSchema } = require('../models/userModel')
const { safetyDeviceApiConfig, appUrl } = require('../config/config')
const { notify } = require('../lib/firebase/fmc')
const Occurrence = mongoose.model('Occurrence', OccurrenceSchema)
const User = mongoose.model('User', UserSchema)

const { baseUri } = safetyDeviceApiConfig

exports.addOccurrence = async (req, res) => {

  const newOccurrence = new Occurrence(req.body)

  const occurrence = await newOccurrence.save()

  const customer = await User.findById(occurrence.customerID).lean().exec();

  const userList = await User.find({ fmcToken: { $ne: null } }, 'fmcToken').lean().exec()

  const clickAction = appUrl + "#/cliente/" + occurrence.customerID
  const fmcTokenList = userList.map(user => user.fmcToken)
  try {
    const customerName = customer ? customer.name : ''
    await notify(customerName, fmcTokenList, clickAction)
  } catch { }

  res.location(`${baseUri}/occurrences/${occurrence._id}`)
  res.status(201).json(occurrence)
}

exports.getOccurrences = async (req, res) => {

  const { clientId, start, end } = req.query

  let filter = {}


  if (start)
    filter.createdAt = { $gte: start }

  if (end)
    filter.createdAt = { $lte: end }

  if (clientId)
    filter.customerID = clientId

  const occurrences = await Occurrence.find(filter).lean().exec()

  if (!occurrences) throw new NotFoundError()

  for (let i = 0; i < occurrences.length; i++) {

    const item = occurrences[i]

    if (!item.customerID)
      continue

    const customer = await User.findById(item.customerID).lean().exec()
    item.customer = customer
  }

  res.json(occurrences)
}

exports.getOccurrenceByID = async (req, res) => {
  const occurrence = await Occurrence.findById(req.params.occurrenceID).lean().exec()
  if (!occurrence) throw new NotFoundError()

  res.json(occurrence)
}

exports.deleteOccurrenceByClient = async (req, res) => {
  await Occurrence.updateMany({ customerID: req.params.clientId, status: 'AGUARDANDO' }, { status: 'ATENDIDO', updatedAt: Date.now() }).exec()

  res.status(204).send()
}
