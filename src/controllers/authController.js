const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const { UserSchema, validate, prepareBeforeSave } = require('../models/userModel')
const { jwtConfig } = require('../config/config')

const User = mongoose.model('User', UserSchema)

exports.register = async (req, res) => {
  const user = new User(req.body)

  user.hashPassword = await bcrypt.hash(req.body.password, 10)

  const newUser = await user.save()
  newUser.hashPassword = undefined

  res.status(200).json(newUser)
}

exports.login = async (req, res) => {
  const { password, email } = req.body

  const user = await User.findOne({ email }).exec()

  if (!user)
    return res.status(404).send()

  if (!user.comparePassword(password, user.hashPassword))
    return res.status(401).send()

  const token = jwt.sign({ _id: user._id, email: user.email, username: user.userName },
    jwtConfig.secret)
  // , { expiresIn: jwtConfig.exp }

  res.status(200).json({ token: token, userId: user._id, userName: user.name, userEmail: user.email })
}

exports.loginRequired = (req, res, next) => {
  if (req.user) { next() } else res.status(401).send()
}
