const mongoose = require('mongoose')
const { NotFoundError } = require('../lib/customerrors/applicationErrors')
const { UserSchema } = require('../models/userModel')
const { safetyDeviceApiConfig } = require('../config/config')
const { query } = require('express')
const bcrypt = require('bcrypt')

const User = mongoose.model('User', UserSchema)

exports.getProfile = async (req, res) => {
  const id = req.user._id
  const user = await User.findById(id).lean().exec()
  if (!user) throw new NotFoundError()

  res.json(user)
}

exports.updateProfile = async (req, res) => {
  const id = req.user._id
  const { password } = req.body

  if (password.length < 6)
    throw 'Senha deve ter 6 ou mais caracteres!'

  const hashPassword = await bcrypt.hash(password, 10);

  const user = await User.findOneAndUpdate({ _id: id },
    { hashPassword: hashPassword }, { new: true, useFindAndModify: false }).exec()

  if (!user) throw new NotFoundError()

  res.json(user.toResponseModel(user))
}
