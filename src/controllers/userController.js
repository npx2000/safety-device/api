const mongoose = require('mongoose')
const { NotFoundError, BadRequestError } = require('../lib/customerrors/applicationErrors')
const { UserSchema, validate, prepareBeforeSave } = require('../models/userModel')
const { safetyDeviceApiConfig } = require('../config/config')
const { query } = require('express')

const User = mongoose.model('User', UserSchema)

const { baseUri } = safetyDeviceApiConfig

exports.addUser = async (req, res) => {
  validate(req.body)

  const userExist = await User.findOne({ email: req.body.email })

  if (userExist)
    throw new BadRequestError("Existe um usuário com esse e-mail.")

  const newUser = new User(await prepareBeforeSave(req.body))

  const user = await newUser.save()

  res.location(`${baseUri}/users/${user._id}`)
  res.status(201).json(user.toResponseModel(user))
}

exports.getUsers = async (req, res) => {

  const userType = req.query.userType

  let filter = {}

  if (userType)
    filter.type = userType

  let limit = 100

  if (req.query.limit)
    limit = +req.query.limit

  const users = await User.find(filter).sort({ createdAt: -1 }).limit(limit).lean().exec()
  if (!users) throw new NotFoundError()

  res.json(users)
}

exports.getUserByID = async (req, res) => {
  const user = await User.findById(req.params.userID).lean().exec()
  if (!user) throw new NotFoundError()

  res.json(user)
}

exports.updateUser = async (req, res) => {
  validate(req.body)
  const userToSave = await prepareBeforeSave(req.body)

  const user = await User.findOneAndUpdate({ _id: req.params.userID },
    userToSave, { new: true, useFindAndModify: false }).exec()

  if (!user) throw new NotFoundError()

  res.json(user.toResponseModel(user))
}

exports.deleteUser = async (req, res) => {
  await User.deleteOne({ _id: req.params.userID }).exec()

  res.status(204).send()
}

exports.getDashboard = async (req, res) => {

  let total = 0
  let active = 0
  let inactive = 0

  total = await User.countDocuments({ type: 2 }).exec()
  active = await User.countDocuments({ type: 2, status: 1 }).exec()
  inactive = await User.countDocuments({ type: 2, status: 2 }).exec()

  res.json({ clientTotal: total, clientActive: active, clientInactive: inactive })
}

exports.updateUserFCMToken = async (req, res) => {

  const user = await User.findOneAndUpdate(
    { _id: req.params.userID },
    { fmcToken: req.body.fmcToken },
    { useFindAndModify: false })
    .exec()

  res.location(`${baseUri}/users/${user._id}`)
  res.status(201).json(user.toResponseModel(user))
}
