/* eslint-disable no-underscore-dangle */
const mongoose = require('mongoose')
const { Schema } = mongoose

const OccurrenceSchema = new Schema({
  awsID: { type: String },
  customerID: { type: mongoose.Types.ObjectId, ref: 'User' },
  latitude: { type: Number },
  longitude: { type: Number },
  status: { type: String, default: 'AGUARDANDO' }
}, { timestamps: true })

module.exports = {
  OccurrenceSchema
}
