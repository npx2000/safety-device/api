/* eslint-disable no-underscore-dangle */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { BadRequestError } = require('../lib/customerrors/applicationErrors');

const { Schema } = mongoose;

const statusDescription = { 1: 'Ativo', 2: 'Bloqueado' };
const typesDescription = { 1: 'Administrador', 2: 'Cliente' };

const UserSchema = new Schema({
  name: { type: String, trim: true },
  login: { type: String },
  password: { type: String },
  hashPassword: { type: String },
  status: { type: Number },
  statusDescription: { type: String },
  email: { type: String },
  type: { type: Number },
  typeDescription: { type: String },
  address: String,
  state: String,
  city: String,
  phone: String,
  document: String,
  zipCode: String,
  region: String,
  complement: String,
  latitude: Number,
  longitude: Number,
  ioTButtonId: String,
  cell: String,
  number: String,
  fmcToken: String
}, { timestamps: true });

const validate = (user) => {
  const errors = [];

  if (!statusDescription[user.status]) {
    errors.push({ status: 'Situação informada não existe' });
  }

  if (!typesDescription[user.type]) {
    errors.push({ type: 'Tipo informado não existe' });
  }

  if (errors.length > 0) {
    throw new BadRequestError('', errors);
  }
};

const prepareBeforeSave = async (user) => {
  const newUser = { ...user };
  newUser.statusDescription = statusDescription[user.status];
  newUser.typeDescription = typesDescription[user.type];

  if (user.password) {
    newUser.hashPassword = await bcrypt.hash(user.password, 10);
  }

  return newUser;
};

UserSchema.methods.toResponseModel = (user) => {
  const userResponse = user;
  userResponse.password = '*****';
  userResponse.hashPassword = undefined;
  return userResponse;
};

UserSchema.methods.comparePassword = (password, hashPassword) => bcrypt.compareSync(password, hashPassword);

module.exports = {
  UserSchema,
  validate,
  prepareBeforeSave,
};
