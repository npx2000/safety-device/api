const axios = require('axios')
const { firebaseConfig } = require('../../config/config');

const notify = async (customerName, tokenList, clickAction) => {

  const notification = {
    title: 'DSS - Botão Pânico',
    body: `Ocorrencia para ${customerName}`,
    click_action: clickAction
  }

  tokenList.forEach(fmcToken => {

    const to = fmcToken

    axios.post(firebaseConfig.fmcUrl, { notification, to }, {
      headers: {
        Host: firebaseConfig.fmcHost,
        Authorization: `key=${firebaseConfig.fmcKey}`,
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        // console.log('fmc', response)
      })
      .catch((error) => {
        console.error('fmc', error)
      })

  })
}

module.exports = {
  notify,
}
